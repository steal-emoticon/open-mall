/**
 * @filename:TbBrandServiceImpl 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service.impl;

import com.xiaopin.goods.entity.TbBrand;
import com.xiaopin.goods.mapper.TbBrandDao;
import com.xiaopin.goods.service.TbBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**   
 * @Description:品牌信息服务实现
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Service
public class TbBrandServiceImpl extends ServiceImpl<TbBrandDao, TbBrand> implements TbBrandService {

    @Autowired
    private TbBrandDao tbBrandDao;

    /**
     * 根据商品分类名称查询品牌列表
     *
     * @param categoryName
     * @return
     */
    @Override
    public List<Map> findBrandListByCategoryName(String categoryName) {
        return tbBrandDao.findBrandListByCategoryName(categoryName);
    }




}