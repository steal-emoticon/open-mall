/**
 * @filename:TbCategoryService 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service;

import com.xiaopin.goods.entity.TbCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description:分类信息服务层
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
public interface TbCategoryService extends IService<TbCategory> {

	
}