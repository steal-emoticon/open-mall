/**
 * @filename:TbSpecServiceImpl 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service.impl;

import com.xiaopin.goods.entity.TbSpec;
import com.xiaopin.goods.mapper.TbSpecDao;
import com.xiaopin.goods.service.TbSpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**   
 * @Description:规格信息服务实现
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Service
public class TbSpecServiceImpl  extends ServiceImpl<TbSpecDao, TbSpec> implements TbSpecService {
    @Autowired
    private TbSpecDao tbSpecDao;
    /**
     * 通过分类名称查询规格列表
     *
     * @param categoryName
     * @return
     */
    @Override
    public List<Map> findSpecListByCategoryName(String categoryName) {
        List<Map> specList = tbSpecDao.findSpecListByCategoryName(categoryName);
        for(Map spec:specList){
            //规格选项列表
            String[] options = ((String) spec.get("options")).split(",");
            spec.put("options",options);
        }
        return specList;
    }
}