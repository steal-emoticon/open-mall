/**
 * @filename:TbSpecService 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service;

import com.xiaopin.goods.entity.TbSpec;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description:规格信息服务层
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
public interface TbSpecService extends IService<TbSpec> {
    /**
     * 通过分类名称查询规格列表
     * @param categoryName
     * @return
     */
    List<Map> findSpecListByCategoryName(String categoryName);
	
}