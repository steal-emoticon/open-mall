/**
 * @filename:TbBrandDao 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaopin.goods.entity.TbBrand;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**   
 * @Description:
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Mapper
public interface TbBrandDao extends BaseMapper<TbBrand> {
    /**
     * 根据分类名称查询品牌列表
     * @param categoryName
     * @return
     */
    @Select("SELECT name,image FROM tb_brand WHERE id IN " +
            "(SELECT brand_id FROM tb_category_brand WHERE category_id IN " +
            "(SELECT id FROM tb_category WHERE NAME=#{name}) )order by seq")
    public List<Map> findBrandListByCategoryName(@Param("name") String categoryName);

}
