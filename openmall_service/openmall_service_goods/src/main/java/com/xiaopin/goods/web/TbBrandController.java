/**
 * @filename:TbBrandController 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.web;

import com.xiaopin.entity.Result;
import com.xiaopin.entity.StatusCode;
import com.xiaopin.goods.aid.AbstractController;
import com.xiaopin.goods.entity.TbBrand;
import com.xiaopin.goods.service.TbBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.List;
import java.util.Map;

/**
 *
 * <p>说明： 品牌信息API接口层</P>
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * @time 2021年9月19日
 */
@Api(description = "品牌信息", value = "品牌信息")
@RestController
@RequestMapping("/goods/tbBrand")
public class TbBrandController extends AbstractController<TbBrandService, TbBrand> {

    @Autowired
    private TbBrandService brandService;
    /**
     * 根据分类名称查询品牌列表
     * @param category
     * @return
     */
    @GetMapping("/category/{category}")
    public Result findBrandListByCategoryName(@PathVariable String category){
        System.out.println(category);
        List<Map> brandList = brandService.findBrandListByCategoryName(category);
        return new Result(true, StatusCode.OK, "查询成功", brandList);
    }



}