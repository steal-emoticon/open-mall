/**
 * @filename:TbSpecController 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.web;

import com.xiaopin.entity.Result;
import com.xiaopin.entity.StatusCode;
import com.xiaopin.goods.aid.AbstractController;
import com.xiaopin.goods.entity.TbSpec;
import com.xiaopin.goods.service.TbCategoryService;
import com.xiaopin.goods.service.TbSpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.List;
import java.util.Map;

/**
 *
 * <p>说明： 规格信息API接口层</P>
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * @time    2021年9月22日
 *
 */
@Api(description = "规格信息",value="规格信息" )
@RestController
@RequestMapping("/goods/tbSpec")
public class TbSpecController extends AbstractController<TbSpecService, TbSpec> {
    @Autowired
    private TbSpecService tbSpecService;

    /**
     * 根据商品分类名称查询规格列表
     * @param category
     * @return
     */
    @GetMapping("/category/{category}")
    public Result findSpecListByCategoryName(@PathVariable String category){
        List<Map> specList = tbSpecService.findSpecListByCategoryName(category);
        return new Result(true, StatusCode.OK,"",specList);
    }
}