/**
 * @filename:TbCategoryController 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.web;

import com.netflix.discovery.converters.Auto;
import com.xiaopin.entity.Result;
import com.xiaopin.entity.StatusCode;
import com.xiaopin.goods.aid.AbstractController;
import com.xiaopin.goods.entity.TbCategory;
import com.xiaopin.goods.service.TbCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.List;
import java.util.Map;

/**
 *
 * <p>说明： 分类信息API接口层</P>
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * @time    2021年9月22日
 *
 */
@Api(description = "分类信息",value="分类信息" )
@RestController
@RequestMapping("/goods/tbCategory")
public class TbCategoryController extends AbstractController<TbCategoryService, TbCategory> {

	
}