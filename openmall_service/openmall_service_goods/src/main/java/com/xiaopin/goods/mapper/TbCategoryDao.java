/**
 * @filename:TbCategoryDao 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaopin.goods.entity.TbCategory;
import org.apache.ibatis.annotations.Mapper;

/**   
 * @Description:分类信息数据访问层
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Mapper
public interface TbCategoryDao extends BaseMapper<TbCategory> {

}
