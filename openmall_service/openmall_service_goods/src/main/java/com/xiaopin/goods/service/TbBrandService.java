/**
 * @filename:TbBrandService 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service;

import com.xiaopin.goods.entity.TbBrand;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description:品牌信息服务层
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
public interface TbBrandService extends IService<TbBrand> {
    /**
     * 根据商品分类名称查询品牌列表
     * @param categoryName
     * @return
     */
    List<Map> findBrandListByCategoryName(String categoryName);




}