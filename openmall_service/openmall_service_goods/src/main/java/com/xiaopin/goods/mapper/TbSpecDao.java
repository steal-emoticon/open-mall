/**
 * @filename:TbSpecDao 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaopin.goods.entity.TbSpec;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**   
 * @Description:规格信息数据访问层
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Mapper
public interface TbSpecDao extends BaseMapper<TbSpec> {
    /**
     * 通过分类名称查询规格列表
     * @param categoryName
     * @return
     */
    @Select("SELECT name,options FROM tb_spec WHERE template_id IN " +
            "( SELECT template_id FROM tb_category WHERE NAME=#{categoryName}) order by seq")
    public List<Map> findSpecListByCategoryName(@Param("categoryName") String categoryName);


}
