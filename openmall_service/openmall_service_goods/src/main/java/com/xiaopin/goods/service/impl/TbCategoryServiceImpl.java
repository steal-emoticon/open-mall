/**
 * @filename:TbCategoryServiceImpl 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaopin.goods.entity.TbCategory;
import com.xiaopin.goods.mapper.TbCategoryDao;
import com.xiaopin.goods.service.TbCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**   
 * @Description:品牌信息服务实现
 *
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Service
public class TbCategoryServiceImpl  extends ServiceImpl<TbCategoryDao, TbCategory> implements TbCategoryService {


}