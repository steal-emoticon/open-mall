package com.xiaopin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author ZYJ
 * @date 2021年09月24日 21:49
 * fastDFS微服务不需要配置数据源
 * 需要排除数据源配置，因为spring会自动加载数据源配置，如果不排除该自动配置类，将报没有数据源异常
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableEurekaClient
public class FileApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class);
    }
}
