package com.xiaopin.controller;

import com.xiaopin.file.pojo.FastDfsFile;
import com.xiaopin.file.util.FastDfsClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author ZYJ
 * @date 2021年09月24日 23:49
 */
@RestController
@CrossOrigin
public class FileController {
    /**
     * 上传文件
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        String path = "";
        try {
            path = saveFile(file);
            System.out.println(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    /**
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public String saveFile(MultipartFile multipartFile) throws IOException {
        //1. 获取文件名
        String fileName = multipartFile.getOriginalFilename();
        //2. 获取文件内容
        byte[] content = multipartFile.getBytes();
        //3. 获取文件扩展名
        String ext = "";
        if (fileName != null && !"".equals(fileName)) {
            ext = fileName.substring(fileName.lastIndexOf("."));
        }
        //4. 创建文件实体类对象
        FastDfsFile fastDFSFile = new FastDfsFile(fileName, content, ext);
        //5. 上传
        String[] uploadResults = FastDfsClient.upload(fastDFSFile);
        //6. 拼接上传后的文件的完整路径和名字, uploadResults[0]为组名, uploadResults[1] 为文件名称和路径
        String path = FastDfsClient.getTrackerUrl() + uploadResults[0] + "/" + uploadResults[1];
        //7. 返回
        return path;
    }

    /**
     * 下载文件
     * @return
     */
    @GetMapping("/download")
    public ResponseEntity<byte[]> download(){
        /**
         * 组名
         */
        String groupName = "group1";
        /**
         * 文件路径
         * 需要根据上传文件返回的id进行获取
         */
        // TODO: 2021/9/25 需要改动文件路径
        String filePath = "M00/00/00/wKjIgF9UNCeAZEAQAAG4YFJxjFY33..jpg";
        byte[] content = FastDfsClient.download(groupName,filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment",new String("黑kkb.jpg".getBytes(StandardCharsets.UTF_8),StandardCharsets.ISO_8859_1));
        return new ResponseEntity<>(content, headers, HttpStatus.CREATED);
    }
}


