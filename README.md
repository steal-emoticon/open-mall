# OpenMall

#### 介绍
一个基于springcloud开发的开源商城前台系统，面向c端，大家多给给star吧，欢迎大家star和参与开发。仓库在迭代开发中，完整项目将会使用的技术主要有，springboot+springcloud+elasticsearch+redis+mysql+rocketmq+elk+openresty+fastDFS等等，主要解决的分布式问题有分布式id，多级缓存，分布式事务，分布式锁，链路追踪等等。

主页
    Gitee地址：https://gitee.com/steal-emoticon/open-mall

开发者规范：
    1.master分支是保护分支，不允许直接提交代码
    2.开发者应本地拉取最新代码，创建本地分支，分支命名规则如下：openmall-姓名-日期-分支功能，比如openmall-zyj-0919-fixbug

代码提交commit message规范：
    init：模块初始化
    feat： 新增 feature
    fix: 修复 bug
    docs: 仅仅修改了文档，比如 README, CHANGELOG, CONTRIBUTE等等
    style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
    refactor: 代码重构，没有加新功能或者修复 bug
    perf: 优化相关，比如提升性能、体验
    test: 测试用例，包括单元测试、集成测试等
    chore: 改变构建流程、或者增加依赖库、工具等
    revert: 回滚到上一个版本

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
