/**
 * @filename:TbCategory 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;

/**   
 * @Description:品牌信息实体类
 * 
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TbCategory extends Model<TbCategory> {

	private static final long serialVersionUID = 1632277277094L;
	
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(name = "id" , value = "分类ID")
	private Integer id;
    
	@ApiModelProperty(name = "name" , value = "分类名称")
	private String name;
    
	@ApiModelProperty(name = "goodsNum" , value = "商品数量")
	private Integer goodsNum;
    
	@ApiModelProperty(name = "isShow" , value = "是否显示")
	private String isShow;
    
	@ApiModelProperty(name = "isMenu" , value = "是否导航")
	private String isMenu;
    
	@ApiModelProperty(name = "seq" , value = "排序")
	private Integer seq;
    
	@ApiModelProperty(name = "parentId" , value = "上级ID")
	private Integer parentId;
    
	@ApiModelProperty(name = "templateId" , value = "模板ID")
	private Integer templateId;
    

	@Override
	public Serializable pkVal() {
        return this.id;
    }
}
