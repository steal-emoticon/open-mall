/**
 * @filename:TbSpec 2021年9月22日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**   
 * @Description:规格信息实体类
 * 
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TbSpec extends Model<TbSpec> {

	private static final long serialVersionUID = 1632278670024L;
	
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(name = "id" , value = "ID")
	private Integer id;
    
	@ApiModelProperty(name = "name" , value = "名称")
	private String name;
    
	@ApiModelProperty(name = "options" , value = "规格选项")
	private String options;
    
	@ApiModelProperty(name = "seq" , value = "排序")
	private Integer seq;
    
	@ApiModelProperty(name = "templateId" , value = "模板ID")
	private Integer templateId;
    

	@Override
	public Serializable pkVal() {
        return this.id;
    }
}
