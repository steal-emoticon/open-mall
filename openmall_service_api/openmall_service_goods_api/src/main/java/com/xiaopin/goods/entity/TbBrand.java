/**
 * @filename:TbBrand 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.goods.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;

/**   
 * @Description:
 * 
 * @version: 1.0-SNAPSHOT
 * @author: ZYJ
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TbBrand extends Model<TbBrand> {

	private static final long serialVersionUID = 1631999249251L;
	
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(name = "id" , value = "品牌id")
	private Integer id;
    
	@ApiModelProperty(name = "image" , value = "品牌图片地址")
	private String image;
    
	@ApiModelProperty(name = "letter" , value = "品牌的首字母")
	private String letter;
    
	@ApiModelProperty(name = "name" , value = "品牌名称")
	private String name;
    
	@ApiModelProperty(name = "seq" , value = "排序")
	private Integer seq;
    

	@Override
	public Serializable pkVal() {
        return this.id;
    }
}
