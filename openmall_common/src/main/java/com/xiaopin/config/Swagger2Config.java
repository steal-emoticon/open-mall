/**
 * @filename:TbBrandController 2021年9月19日
 * @project openmall  1.0-SNAPSHOT
 * Copyright(c) 2021 ZYJ Co. Ltd.
 * All right reserved. 
 */
package com.xiaopin.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger核心配置文件
 * ========================
 * @author ZYJ 
 * @Date   2021年9月19日
 * ========================
 * TODO: 2021/9/25 访问 http://localhost:9011/swagger-ui.html 报错, 需要修复bug
 */

@Configuration
@EnableSwagger2
public class Swagger2Config {

	@Bean
	public Docket WebApiConfig(){
		return  new Docket(DocumentationType.SWAGGER_2)
				.groupName("webApi")
				.apiInfo(webApiInfo())
				.select()
				.paths(Predicates.not(PathSelectors.regex("/admin/.*")))
				.paths(Predicates.not(PathSelectors.regex("/error.*")))
				.build();
	}

	@Bean
	public Docket adminApiConfig(){

		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("adminApi")
				.apiInfo(adminApiInfo())
				.select()
				.paths(Predicates.and(PathSelectors.regex("/admin/.*")))
				.build();

	}
	private ApiInfo webApiInfo(){

		return new ApiInfoBuilder()
				.title("openmall开源商城API文档")
				.description("本文档描述了openmall开源商城微服务接口定义")
				.version("1.0-SNAPSHOT")
				.contact(new Contact("zyj", "https://gitee.com/steal-emoticon/open-mall", "1913829827@qq.com"))
				.build();
	}

	private ApiInfo adminApiInfo(){

		return new ApiInfoBuilder()
				.title("openmall开源商城API文档")
				.description("本文档描述了openmall开源商城微服务接口定义")
				.version("1.0-SNAPSHOT")
				.contact(new Contact("zyj", "https://gitee.com/steal-emoticon/open-mall", "1913829827@qq.com"))
				.build();
	}
}
