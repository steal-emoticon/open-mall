package com.xiaopin.entity;

import lombok.Data;

import java.util.List;

/**
 * @author ZYJ
 * @date 2021年09月15日 16:39
 */
@Data
public class PageResult<T> {
    /**
     * 总记录数
     */
    private Long total;
    /**
     * 记录
     */
    private List<T> rows;

}
